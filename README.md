# "*Userdata*"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Um H2 als Datenbank zum Persistieren der Daten festzulegen, muss folgendes im *application.properties* File festgelegt werden:

```properties
# Datasource
spring.datasource.url=jdbc:h2:file:./data/user_db
spring.datasource.username=admin
spring.datasource.password=userdata
spring.datasource.driver-class-name=org.h2.Driver
# Use "create" or "create-drop" when you wish to recreate database on restart;
# use "update" or "validate" when data is to be kept.
spring.jpa.hibernate.ddl-auto=create
```

*Auszug aus application.properties*

Wenn man statt dem Modus `create`, `update` verwendet, kann man aber trotzdem manuell alle Daten der Datenbank vor dem Start leeren:

```java
// clear all data before start
repository.deleteAll();
```

*Auszug aus Application*

Für die Aufgabe muss die Customer Klasse an die Bedürfnisse angepasst werden, dazu müssen zwei neue Attribute festgelegt werden:

```java
private LocalDate birthDay;

@Email
private String email;

// Setter and Getter ...
```

*Auszug aus Customer*

Außerdem habe ich Setter/Getter-Methoden hinzugefügt und neue Konstruktoren erstellt.

In der Klasse *CustomerEditor* müssen neue Komponenten für ein Email-Feld und einen Datepicker hinzugefügt werden. Im Konstruktor wird mithilfe eines binders das Verhalten aller Komponenten bestimmt, also ob sie beispielweise notwendig sind oder irgendwelche Voraussetzungen besitzen. Voraussetzungen können mithilfe von Validatoren implementiert werden, die schauen ob die User-Eingaben den Richtlinien entsprechen. 

```java
EmailField emailField = new EmailField("Email");
DatePicker datePicker = new DatePicker("Birth day");
...
@Autowired
public CustomerEditor(CustomerRepository repository) {
    ...
    firstName.setRequired(true);
	lastName.setRequired(true);
	...
	// add elements, new is the datePicker
	add(firstName, lastName, emailField, datePicker, actions);
	...
	// set validators
	binder.forField(emailField)
        .asRequired("Field can not be empty") 
        // Check if valid email-address
        .withValidator(new EmailValidator(
            "This doesn't look like a valid email address"
        )).bind(Customer::getEmail, Customer::setEmail);
    
    binder.forField(firstName)
        .asRequired("Field can not be empty")
        .withValidator(new StringLengthValidator(
            "Please add the first name", 1, null))
        .bind(Customer::getFirstName, Customer::setFirstName);

	binder.forField(lastName)
        .asRequired("Field can not be empty")
        .withValidator(new StringLengthValidator(
            "Please add the last name", 1, null))
        .bind(Customer::getLastName, Customer::setLastName);
				
	binder.forField(datePicker)
        // Check if valid birthday (date not in the future)
        .withValidator(new DateRangeValidator(
            "This doesn't look like a valid birthday", null, LocalDate.now()))
        .bind(Customer::getBirthDay, Customer::setBirthDay);
```

*Auszug aus CustomEditor*

Damit wirklich nur valide Eingaben gespeichert werden, und nicht nur Meldungen angezeigt werden, musss in der `save()`-Methode mit einer *If*-Anweisung geschaut werden, ob alle Felder valide Werte besitzen. Erst dann soll gespeichert werden.

```java
void save() {
    // Check first if field contain valid values,
    // if not, don´t save changes
    if (binder.writeBeanIfValid(customer)) {
        repository.save(customer);
        changeHandler.onChange();
    }
}
```

*Auszug aus CustomEditor*

---

### Testen

Zu aller erst schaute ich wie die vorhanden Tests funktionierten. Zu meinen Bedauern funktionierten einige überhaupt nichts, sie wurfen eine `java.lang.reflect.InvocationTargetException`. Ich fand im Internet keine wirkliche Lösung für das Problem, ich wusste aber, dass der Fehler beim Erstellen des DatePickers geworfen wird. Ich debuggte eine Zeit lang und stoß schließlich auf den Fehler:

![Fehler](img/BadDatePicker.PNG)

*Auszug aus DatePicker - Vaadin*

Der Konstruktor eines DatePickers holt sich das derzeitige *UI*-Objekt um aus seinen Daten nutzerspezifische Informationen (wahrscheinlich Region oder verwendete Textcodierung) herauszuholen und diese auf das DatePicker-Objekt anzuwenden. 

Eine *UI* in Vaadin ist die höchste Komponente aller, sozusagen das Grundgerüst. Das Problem, beim Testen werden immer wieder die Klassen `MainView` und `CustomEditor` mit `new` erstellt um neue Logik-Tests auszuführen. Das verhindert Spring alles schön aufzulösen und auch eine UI aufzubauen, dies heißt defacto, dass beim Testen nie ein UI-Objekt existiert. Somit schlägt der Aufruf `getLocale()` beim Konstruktor von DatePicker fehl, weil `UI.getCurrent()` `null` zurückgibt. Dieses Problem kann man aber lösen, indem man einfach bevor jedem Test ein UI-Objekt erstellt und dieses global setzt.

```java
@Before
public void setup() {
    // A UI needs to be set manually, because through construction with new, Spring will not
    // configure Vaadin.
    // A UI is needed, because the DatePicker relies to get locale region information
    // through the UI in the constructor. With no UI set this lead to a
    // java.lang.reflect.InvocationTargetException because a method is called from a null object.
    UI testUI = new UI();
    UI.setCurrent(testUI);
    this.editor = new CustomerEditor(this.repository);
    this.mainView = new MainView(this.repository, editor);
}
```

*Auszug aus MainViewTests*

Jetzt funktionierten die vorhandenen Tests mit ein paar kleinen weiteren Erweiterungen endlich.

Meine eigenen Tests habe ich in der Klasse `TaskTest` definiert. In diesen prüfe ich, ob die CRUD-Operationen (Create, Read, Update, Delete) korrekt von der Datenbank und der GUI behandelt werden.

Der schwierigste Test war zu schauen, ob eine Update-Operation mit falschen Werten fehlschlägt. Dazu muss man einen Klick im Grid simulieren, Werte einfügen und per cancel-Simulation wieder auf den Ursprungszustand setzen. 

```java
/**
* Tests if updating the last {@link Customer} with not valid values is not successful.
*/
@Test
public void testUpdateLastConsumerWithInvalidInput() {
    int dbCount = (int) repository.count(); // Get database count
    List<Customer> customers = getCustomersInGrid();
    Customer last = customers.get(customers.size()-1);
    String firstName = last.getFirstName();
    String lastName = last.getLastName();
    String email = last.getEmail();
    // Select Customer from the grid
    this.mainView.grid.select(last);
    this.editor.lastName.setValue("");
    this.editor.firstName.setValue("");
    this.editor.emailField.setValue("update");
    this.editor.save();
    // Revert to last valid value
    this.editor.cancel.click();
    this.editor.save();

    then(repository.count()).isEqualTo(dbCount);
    then(getCustomersInGrid()).hasSize(dbCount);
    then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
        .extracting("firstName", "lastName", "email")
        .containsExactly(firstName, lastName, email);
}
```

*Auszug aus TaskTests*

Bei anderen Tests dagegen habe ich öfter auf GUI-Simulation verzichtet, sondern mehr auf die Methode `editCustomer`. Mit dieser kann man komfortabel neue Benutzer erstellen und auch einen bestehenden Benutzer "in den Vordergrund der GUI" setzen, um eine Delete-Operation auszuführen.

## Quellen

* [Vaadin - Form Example; 29.04.2019; online](https://vaadin.com/components/vaadin-form-layout/java-examples)
* [Vaadin - Binder; 29.04.2019; online](https://vaadin.com/docs/flow/binding-data/tutorial-flow-components-binder-beans.html)
* [Vaadin - Validation; 29.04.2019; online](https://vaadin.com/docs/v10/flow/binding-data/tutorial-flow-components-binder-validation.html)
* [Vaadin - TextField + EmailField; 29.04.2019; online](https://vaadin.com/components/vaadin-text-field/java-examples)
* [Vaadin - DatePicker; 29.04.2019; online](https://vaadin.com/components/vaadin-date-picker/java-examples)
* [Spring - Getting started with Vaadin; 29.04.2019; online](https://spring.io/guides/gs/crud-with-vaadin/)
* [H2 - Spring Configuration; 29.04.2019; online](https://dzone.com/articles/integrate-h2-database-in-your-spring-boot-applicat)
* [String zu LocalDate; 29.04.2019; online](https://examples.javacodegeeks.com/core-java/java-8-convert-string-localdate-example/)
* [Testing - Omitting Inject Mocks; 03.05.2019; online](https://stackoverflow.com/questions/16467685/difference-between-mock-and-injectmocks)