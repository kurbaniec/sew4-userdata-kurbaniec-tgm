package hello;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.DateRangeValidator;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in MainView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX. <br>
 * Source of initial code:  <a href="https://spring.io/guides/gs/crud-with-vaadin/">link</a>
 * @author Kacper Urbaniec
 * @version 2019-04-29
 */

@SpringComponent
@UIScope
public class CustomerEditor extends VerticalLayout implements KeyNotifier {

	private final CustomerRepository repository;

	/**
	 * The currently edited customer
	 */
	private Customer customer;

	/* Fields to edit properties in Customer entity */
	TextField firstName = new TextField("First name");
	TextField lastName = new TextField("Last name");
	EmailField emailField = new EmailField("Email");
	/* Datepicker for date input */
	DatePicker datePicker = new DatePicker("Birth day");

	/* Action buttons */
	Button save = new Button("Save", VaadinIcon.CHECK.create());
	Button cancel = new Button("Cancel");
	Button delete = new Button("Delete", VaadinIcon.TRASH.create());
	HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

	Binder<Customer> binder = new Binder<>(Customer.class);
	private ChangeHandler changeHandler;

	@Autowired
	public CustomerEditor(CustomerRepository repository) {
		// define repository, used to access database
		this.repository = repository;

		// add elements, new is the datePicker
		add(firstName, lastName, emailField, datePicker, actions);

		// set validators
		binder.forField(emailField)
				.asRequired("Field can not be empty")
				// Check if valid email-address
				.withValidator(new EmailValidator(
						"This doesn't look like a valid email address"
				)).bind(Customer::getEmail, Customer::setEmail);


		binder.forField(firstName)
				.asRequired("Field can not be empty")
				.withValidator(new StringLengthValidator(
						"Please add the first name", 1, null))
				.bind(Customer::getFirstName, Customer::setFirstName);

		binder.forField(lastName)
				.asRequired("Field can not be empty")
				.withValidator(new StringLengthValidator(
						"Please add the last name", 1, null))
				.bind(Customer::getLastName, Customer::setLastName);


		binder.forField(datePicker)
				// Check if valid birthday (date not in the future)
				.withValidator(new DateRangeValidator(
						"This doesn't look like a valid birthday", null, LocalDate.now()))
				.bind(Customer::getBirthDay, Customer::setBirthDay);

		// bind using naming convention
		binder.bindInstanceFields(this);

		// Configure and style components
		setSpacing(true);

		save.getElement().getThemeList().add("primary");
		delete.getElement().getThemeList().add("error");

		addKeyPressListener(Key.ENTER, e -> save());

		// wire action buttons to save, delete and reset
		save.addClickListener(e -> save());
		delete.addClickListener(e -> delete());
		cancel.addClickListener(e -> editCustomer(customer));
		setVisible(false);
	}

	void delete() {
		repository.delete(customer);
		changeHandler.onChange();
	}

	void save() {
		// Check first if field contain valid values,
		// if not, don´t save changes
		if (binder.writeBeanIfValid(customer)) {
			repository.save(customer);
			changeHandler.onChange();
		}
	}

	public interface ChangeHandler {
		void onChange();
	}

	public final void editCustomer(Customer c) {
		if (c == null) {
			setVisible(false);
			return;
		}
		final boolean persisted = c.getId() != null;
		if (persisted) {
			// Find fresh entity for editing
			customer = repository.findById(c.getId()).get();
		}
		else {
			customer = c;
		}
		cancel.setVisible(persisted);

		// Bind customer properties to similarly named fields
		// Could also use annotation or "manual binding" or programmatically
		// moving values from fields to entities before saving
		// set validators
		binder.setBean(customer);

		setVisible(true);

		// Focus first name initially
		firstName.focus();
	}

	public void setChangeHandler(ChangeHandler h) {
		// ChangeHandler is notified when either save or delete
		// is clicked
		changeHandler = h;
	}

}
