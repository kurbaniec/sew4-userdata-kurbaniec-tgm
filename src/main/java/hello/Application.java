package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Simple application that presents database data via the Vaadin-Framework.
 * <br>
 * Source of initial code:  <a href="https://spring.io/guides/gs/crud-with-vaadin/">link</a>
 * @author Kacper Urbaniec
 * @version 2019-04-29
 */
@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	/**
	 * Clear the database on start and fill with some default values.
	 * @param repository interface to access the database and execute commands
	 */
	@Bean
	public CommandLineRunner loadData(CustomerRepository repository) {
		return (args) -> {
			// clear all data before start
			repository.deleteAll();

			// save a couple of customers
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			repository.save(new Customer("Jack", "Bauer", "jbauer@bauer.com",
					LocalDate.parse("1990-01-01", formatter)));
			repository.save(new Customer("Chloe", "O'Brian", "cbrian@gmail.com",
					LocalDate.parse("1995-06-01", formatter)));
			repository.save(new Customer("Kim", "Bauer", "kbauer@bauer.com",
					LocalDate.parse("1990-01-02", formatter)));
			repository.save(new Customer("David", "Palmer", "dpalmer@gmail.com",
					LocalDate.parse("1980-08-10", formatter)));
			repository.save(new Customer("Michelle", "Dessler", "mdessler@yahoo.com",
					LocalDate.parse("1992-02-03", formatter)));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			/*Customer customer = repository.findById(1L).get();
			log.info("Customer found with findOne(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");*/

			// fetch customers by last name
			log.info("Customer found with findByLastNameStartsWithIgnoreCase('Bauer'):");
			log.info("--------------------------------------------");
			for (Customer bauer : repository
					.findByLastNameStartsWithIgnoreCase("Bauer")) {
				log.info(bauer.toString());
			}
			log.info("");
		};
	}

}
