package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import java.time.LocalDate;


/**
 * Defines a JPA-Entity Customer <br>
 * This entity will be persisted in a H2 database.
 * <br>
 * Source of initial code:  <a href="https://spring.io/guides/gs/crud-with-vaadin/">link</a>
 * @author Kacper Urbaniec
 * @version 2019-04-29
 */
@Entity
public class Customer {

	@Id
	@GeneratedValue
	private Long id;  // Generated automatically from the database

	private String firstName;

	private LocalDate birthDay;

	@Email
	private String email;

	private String lastName;

	protected Customer() {
	}

	// Legacy constructor.
	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	// This constructor should be used for a valid Customer.
	public Customer(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	// This constrcutor should be used for a valid Customer with all information.
	public Customer(String firstName, String lastName, String email, LocalDate birthDay) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDay = birthDay;
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(LocalDate birthDay) {
		this.birthDay = birthDay;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return String.format("Customer[id=%d, firstName='%s', lastName='%s', email='%s', birthday='%s']", id,
				firstName, lastName, email, birthDay.toString());
	}



}
