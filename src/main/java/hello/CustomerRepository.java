package hello;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Simple Repository for accessing database functionality.
 * Many functions are implemented by default.
 * <br>
 * Source of initial code:  <a href="https://spring.io/guides/gs/crud-with-vaadin/">link</a>
 * @author Kacper Urbaniec
 * @version 2019-04-29
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	List<Customer> findByLastNameStartsWithIgnoreCase(String lastName);
}
