package hello;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.server.VaadinRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * My own tests for checking the newly added functionalities.
 * @author Kacper Urbaniec
 * @version 2019-05-03
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskTests.Config.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class TaskTests {

    @Autowired
    CustomerRepository repository;

    VaadinRequest vaadinRequest = Mockito.mock(VaadinRequest.class);

    CustomerEditor editor;

    MainView mainView;

    @Before
    public void setup() {
        // A UI needs to be set manually, because through construction with new, Spring will not
        // configure Vaadin.
        // A UI is needed, because the DatePicker relies to get locale region information
        // through the UI in the constructor. With no UI set this lead to a
        // java.lang.reflect.InvocationTargetException because a method is called from a null object.
        UI testUI = new UI();
        UI.setCurrent(testUI);
        this.editor = new CustomerEditor(this.repository);
        this.mainView = new MainView(this.repository, editor);
    }

    /**
     * Test if amount of people showing in the view matches the database count.
     */
    @Test
    public void testView() {
        int dbCount = (int) repository.count(); // Get database count
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Test if newly added {@link Customer} with valid parameters is added to
     * the database and view.
     */
    @Test
    public void testValidInsert() {
        int dbCount = (int) repository.count(); // Get database count
        this.editor.editCustomer(new Customer("Kacper", "Urbaniec",
                "kurbaniec@gmail.com"));
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount + 1);
        then(getCustomersInGrid()).hasSize(dbCount + 1);
        then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
                .extracting("firstName", "lastName", "email")
                .containsExactly("Kacper", "Urbaniec", "kurbaniec@gmail.com");
    }

    /**
     * Test if newly added {@link Customer} with valid parameters is added to
     * the database and view. Now also contains a birthday.
     */
    @Test
    public void testValidInsertWithBirthday() {
        int dbCount = (int) repository.count(); // Get database count
        this.editor.editCustomer(new Customer("Kacper", "Urbaniec",
                "kurbaniec@gmail.com", LocalDate.parse("2000-11-12")));
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount + 1);
        then(getCustomersInGrid()).hasSize(dbCount + 1);
        then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
                .extracting("firstName", "lastName", "email", "birthDay")
                .containsExactly("Kacper", "Urbaniec", "kurbaniec@gmail.com", LocalDate.parse("2000-11-12"));
    }

    /**
     * Test if invalid insert (missing every requirements) is not saved anywhere.
     */
    @Test
    public void testInvalidInsertMissingEverything() {
        int dbCount = (int) repository.count(); // Get database count
        this.editor.editCustomer(new Customer());
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Test if invalid insert (missing firstname) is not saved anywhere.
     */
    @Test
    public void testInvalidInsertMissingFirstName() {
        int dbCount = (int) repository.count(); // Get database count
        Customer c = new Customer();
        c.setLastName("Test");
        c.setEmail("test@gmail.com");
        this.editor.editCustomer(c);
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Test if invalid insert (missing lastname) is not saved anywhere.
     */
    @Test
    public void testInvalidInsertMissingLastName() {
        int dbCount = (int) repository.count(); // Get database count
        Customer c = new Customer();
        c.setFirstName("Test");
        c.setEmail("test@gmail.com");
        this.editor.editCustomer(c);
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Test if invalid insert (missing email) is not saved anywhere.
     */
    @Test
    public void testInvalidInsertMissingEmail() {
        int dbCount = (int) repository.count(); // Get database count
        Customer c = new Customer();
        c.setFirstName("Test");
        c.setLastName("Test");
        this.editor.editCustomer(c);
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Test if invalid insert (invalid email) is not saved anywhere.
     */
    @Test
    public void testInvalidEmail() {
        int dbCount = (int) repository.count(); // Get database count
        Customer c = new Customer("Test", "Test", "Test");
        this.editor.editCustomer(c);
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Test if invalid insert (invalid birthday, in the future) is not saved anywhere.
     */
    @Test
    public void testInvalidBirthday() {
        int dbCount = (int) repository.count(); // Get database count
        Customer c = new Customer("Kacper", "Urbaniec",
                "kurbaniec@gmail.com", LocalDate.now().plusDays(1));
        this.editor.editCustomer(c);
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
    }

    /**
     * Tests if deleting the last {@link Customer} is successful.
     */
    @Test
    public void testDeleteLastCustomer() {
        int dbCount = (int) repository.count(); // Get database count
        List<Customer> customers = getCustomersInGrid();
        Customer last = customers.get(customers.size()-1);
        this.editor.editCustomer(last);
        this.editor.delete();

        then(repository.count()).isEqualTo(dbCount-1);
        then(getCustomersInGrid()).hasSize(dbCount-1);
        then(getCustomersInGrid()).asList().doesNotContain(last);
    }

    /**
     * Tests if updating the last {@link Customer} is successful.
     */
    @Test
    public void testUpdateLastConsumer() {
        int dbCount = (int) repository.count(); // Get database count
        List<Customer> customers = getCustomersInGrid();
        Customer last = customers.get(customers.size()-1);
        // Select Customer from the grid
        this.mainView.grid.select(last);
        this.editor.lastName.setValue("UpdateTest");
        this.editor.firstName.setValue("UpdateTest");
        this.editor.emailField.setValue("update@test.com");
        this.editor.save();
        // Revert to last valid value
        this.editor.cancel.click();
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
        then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
                .extracting("firstName", "lastName", "email")
                .containsExactly("UpdateTest", "UpdateTest", "update@test.com");
    }

    /**
     * Tests if updating the last {@link Customer} with not valid values is not successful.
     */
    @Test
    public void testUpdateLastConsumerWithInvalidInput() {
        int dbCount = (int) repository.count(); // Get database count
        List<Customer> customers = getCustomersInGrid();
        Customer last = customers.get(customers.size()-1);
        String firstName = last.getFirstName();
        String lastName = last.getLastName();
        String email = last.getEmail();
        // Select Customer from the grid
        this.mainView.grid.select(last);
        this.editor.lastName.setValue("");
        this.editor.firstName.setValue("");
        this.editor.emailField.setValue("update");
        this.editor.save();
        // Revert to last valid value
        this.editor.cancel.click();
        this.editor.save();

        then(repository.count()).isEqualTo(dbCount);
        then(getCustomersInGrid()).hasSize(dbCount);
        then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
                .extracting("firstName", "lastName", "email")
                .containsExactly(firstName, lastName, email);
    }


    /**
     * Returns all Customer Objects from the View.
     * @return Customer Objects from the View as a List representation
     */
    private List<Customer> getCustomersInGrid() {
        ListDataProvider<Customer> ldp = (ListDataProvider) mainView.grid.getDataProvider();
        return new ArrayList<>(ldp.getItems());
    }


    @Configuration
    @EnableAutoConfiguration(exclude = com.vaadin.flow.spring.SpringBootAutoConfiguration.class)
    static class Config {

        @Autowired
        CustomerRepository repository;

        @PostConstruct
        public void initializeData() {
            repository.save(new Customer("Jack", "Bauer", "jbauer@bauer.com",
                    LocalDate.parse("1990-01-01")));
            repository.save(new Customer("Chloe", "O'Brian", "cbrian@gmail.com",
                    LocalDate.parse("1995-06-01")));
            repository.save(new Customer("Kim", "Bauer", "kbauer@bauer.com",
                    LocalDate.parse("1990-01-02")));
            repository.save(new Customer("David", "Palmer", "dpalmer@gmail.com",
                    LocalDate.parse("1980-08-10")));
            repository.save(new Customer("Michelle", "Dessler", "mdessler@yahoo.com",
                    LocalDate.parse("1992-02-03")));
        }
    }
}