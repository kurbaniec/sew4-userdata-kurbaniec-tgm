package hello;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.server.VaadinRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.BDDAssertions.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainViewTests.Config.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class MainViewTests {

	@Autowired CustomerRepository repository;

	VaadinRequest vaadinRequest = Mockito.mock(VaadinRequest.class);

	CustomerEditor editor;

	MainView mainView;

	@Before
	public void setup() {
		// A UI needs to be set manually, because through construction with new, Spring will not
		// configure Vaadin.
		// A UI is needed, because the DatePicker relies to get locale region information
		// through the UI in the constructor. With no UI set this lead to a
		// java.lang.reflect.InvocationTargetException because a method is called from a null object.
		UI testUI = new UI();
		UI.setCurrent(testUI);
		this.editor = new CustomerEditor(this.repository);
		this.mainView = new MainView(this.repository, editor);
	}

	@Test
	public void shouldInitializeTheGridWithCustomerRepositoryData() {
		int customerCount = (int) this.repository.count();

		then(mainView.grid.getColumns()).hasSize(5);
		then(getCustomersInGrid()).hasSize(customerCount);
	}

	private List<Customer> getCustomersInGrid() {
		ListDataProvider<Customer> ldp = (ListDataProvider) mainView.grid.getDataProvider();
		return new ArrayList<>(ldp.getItems());
	}

	// Update: Customer now needs a email for a full, correct profile in order
	// to be saved to the database.
	@Test
	public void shouldFillOutTheGridWithNewData() {
		int initialCustomerCount = (int) this.repository.count();

		customerDataWasFilled(editor, "Marcin", "Grzejszczak",
				"marcin@grzejszczak.com");

		this.editor.save();

		then(getCustomersInGrid()).hasSize(initialCustomerCount + 1);

		then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
			.extracting("firstName", "lastName")
			.containsExactly("Marcin", "Grzejszczak");

	}

	@Test
	public void shouldFilterOutTheGridWithTheProvidedLastName() {

		this.repository.save(new Customer("Josh", "Long"));

		mainView.listCustomers("Long");

		then(getCustomersInGrid()).hasSize(1);
		then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
			.extracting("firstName", "lastName")
			.containsExactly("Josh", "Long");
	}

	@Test
	public void shouldInitializeWithInvisibleEditor() {

		then(this.editor.isVisible()).isFalse();
	}

	@Test
	public void shouldMakeEditorVisible() {
		Customer first = getCustomersInGrid().get(0);
		this.mainView.grid.select(first);

		then(this.editor.isVisible()).isTrue();
	}

	// Update: a new parameter called email is needed, because a Customer now
	// needs a email, without is he is not validated for saving.
	private void customerDataWasFilled(CustomerEditor editor, String firstName,
			String lastName, String email) {
		this.editor.firstName.setValue(firstName);
		this.editor.lastName.setValue(lastName);
		this.editor.emailField.setValue(email);
		editor.editCustomer(new Customer(firstName, lastName, email));
	}

	@Configuration
	@EnableAutoConfiguration(exclude = com.vaadin.flow.spring.SpringBootAutoConfiguration.class)
	static class Config {

		@Autowired
		CustomerRepository repository;

		@PostConstruct
		public void initializeData() {
			repository.save(new Customer("Jack", "Bauer", "jbauer@bauer.com",
					LocalDate.parse("1990-01-01")));
			repository.save(new Customer("Chloe", "O'Brian", "cbrian@gmail.com",
					LocalDate.parse("1995-06-01")));
			repository.save(new Customer("Kim", "Bauer", "kbauer@bauer.com",
					LocalDate.parse("1990-01-02")));
			repository.save(new Customer("David", "Palmer", "dpalmer@gmail.com",
					LocalDate.parse("1980-08-10")));
			repository.save(new Customer("Michelle", "Dessler", "mdessler@yahoo.com",
					LocalDate.parse("1992-02-03")));
		}
	}
}
