package hello;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.then;

import com.vaadin.flow.component.UI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.annotation.PostConstruct;

@RunWith(MockitoJUnitRunner.class)
public class CustomerEditorTests {

	private static final String FIRST_NAME = "Marcin";
	private static final String LAST_NAME = "Grzejszczak";
	private static final String EMAIL = "marcin@gzejszczak.com"; // email needed to be added


	@Mock CustomerRepository customerRepository;

	// @InjectMocks can´t be used, because it returns a error
	CustomerEditor editor;

	@Mock CustomerEditor.ChangeHandler changeHandler;

	@Before
	public void init() {
		// A UI needs to be set manually, because through construction with new, Spring will not
		// configure Vaadin.
		// A UI is needed, because the DatePicker relies to get locale region information
		// through the UI in the constructor. With no UI set this lead to a
		// java.lang.reflect.InvocationTargetException because a method is called from a null object.
		UI testUI = new UI();
		UI.setCurrent(testUI);
		MockitoAnnotations.initMocks(this);
		editor = new CustomerEditor(customerRepository);
		editor.setChangeHandler(changeHandler);
	}

	@Test
	public void shouldStoreCustomerInRepoWhenEditorSaveClicked() {
		emptyCustomerWasSetToForm();

		this.editor.firstName.setValue(FIRST_NAME);
		this.editor.lastName.setValue(LAST_NAME);
		this.editor.emailField.setValue(EMAIL); // email needed to be added

		this.editor.save();

		then(this.customerRepository).should().save(argThat(customerMatchesEditorFields()));
	}

	@Test
	public void shouldDeleteCustomerFromRepoWhenEditorDeleteClicked() {
		customerDataWasFilled();

		editor.delete();

		then(this.customerRepository).should().delete(argThat(customerMatchesEditorFields()));
	}

	private void emptyCustomerWasSetToForm() {
		this.editor.editCustomer(new Customer());
	}
	private void customerDataWasFilled() {
		this.editor.editCustomer(new Customer(FIRST_NAME, LAST_NAME));
	}

	private ArgumentMatcher<Customer> customerMatchesEditorFields() {
		return customer -> FIRST_NAME.equals(customer.getFirstName()) && LAST_NAME.equals(customer.getLastName());
	}

}
